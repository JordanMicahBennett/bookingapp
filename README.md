![alt text]( https://bytebucket.org/JordanMicahBennett/bookingapp/raw/21513956b8f2add30ad7f2f5ad9300d08521cbb6/data/miscallaneous/screenshot-0.png)


# DESCRIPTION
This is a scratch written (except for flat ui metro css) platform that permits complex appointment booking for a driving school.

It is complete with the following sections:

a. System side (super user)

b. Driver side (cannot book appointment, can view associated students/relevant appointments to drivers)

c. Student side (can book appointment)

d. Principal side (similar to admin, can book appointments, add users and a lot more, but can't delete users)



# RUNNING THE APP 

Booking app is live at [bookjaa.rf.gd](https://bookjaa.rf.gd/). (Note this is on a free server!)

# RUNNING THE APP/Valid Logins:

An admin user (username: admin@jaa.com, password: admin)

A system user (username: jordanbennett@jaa.com, password: modern2017)

A principal user (username: kamilahanderson@jaa.com, password: Jaadriving@1)

A student user (username: uchiha@gmail.com, password: konoha)

A driver user (username: howardlewis@jaa.com, password: Jaadriving@1)


## USER MANUAL

See the "[Admin Manual](https://bitbucket.org/JordanMicahBennett/bookingapp/src/21513956b8f2add30ad7f2f5ad9300d08521cbb6/Admin%20%20Manual.pdf)" section for details.

## DESIGN DISCUSSION

The design features a layout I call "DupletMaterial design", which is typically expressed as a side bar, together with a central area accessed when each side bar item is toggled, complete with a pair or duplet of components; including a large header, and a body with controls.

![alt text]( https://bytebucket.org/JordanMicahBennett/bookingapp/raw/34723375a512947ca4af9c3c40d811f62858c096/data/miscallaneous/screenshot-1.png)

# RUNNING THE APP (OFFLINE)

You'll have to ensure that all php files in "[data/phps/](https://bitbucket.org/JordanMicahBennett/bookingapp/src/master/data/phps/)" are updated with the appropriate connection details for your online server.


AUTHOR PORTFOLIO
============================================
http://folioverse.appspot.com/


